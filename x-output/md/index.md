---
pageno: 0.01
slug: home
---
   
# <span style="color:red; font-size:1em"><b>O</b>pen <b>D</b>ecision <b>T</b>ool  (<b>ODT</b>)</span> Demonstrator   
   
The Open Decision Tool Demonstrator is a project exploring the tool support for a Structured Decision Making and Management process.  This site provides background on the supported Structured Decision Making and Management process and directions and help in using the tool.   
   
   
   
#### Latest [Blog](./Blog/Blog%20Index.md) Post